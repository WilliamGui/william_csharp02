﻿using System;
using System.IO;

namespace William_csharp02
{
    internal class Program
    {
        private static void Main(string[] arg)
        {
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            Directory.CreateDirectory(desktopPath + "\\William_csharp02");

            string str = "Bila mencintaimu adalah ilusi maka izinkan aku berimajinasi selamanya";
            string[] nama = str.Split(" ");
            foreach (string i in nama)
            {
                File.Create(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) + @"\William_csharp02\" + i + ".txt");
            }
        }
    }
}